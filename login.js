const puppeteer = require('puppeteer');

const login = async (user) => {
  const browser = await puppeteer.launch({headless: true});
  const page = await browser.newPage();
  await page.goto('https://stage-api.feversocial.com/login/enterprise?redirect_uri=https%3A%2F%2Fevents.catwant.tw%2Fpromo%2Fchecklogin%3Fpromoid%3D48%26scenes%3Dpc%26parent%3Dhttps%3A%2F%2Fevents.catwant.tw%2Fpromo-6-48');
  const cookiesObject = await page.cookies()
  console.log(`${user} \n ===============================`)
  console.log(cookiesObject)
  console.log('=============================== \n')
  await browser.close();
};

Promise.all([
    login('1'),
    login('2'),
    login('3'),
    login('4'),
    login('5'),
]);