const puppeteer = require('puppeteer');
  
const login = async ({ name, fbid, email, password }) => {
  const browser = await puppeteer.launch({ headless: false, slowMo: 50 });
  const page = await browser.newPage();
  await page.goto('https://events.catwant.tw/promo/join?promoid=49&scenes=pc&parent=https://events.catwant.tw/promo-6-49', {
    waitUntil: 'networkidle2'
  });
  await page.click("#container")
  await page.click("#e2e-login-button")
  const newPagePromise = new Promise(x => browser.once('targetcreated', target => x(target.page()))); 
  const popup = await newPagePromise;

  await popup.waitForSelector("#email");
  await popup.waitForSelector("#pass");
  await popup.type("#email", email);
  await popup.type("#pass", password);
  await popup.click("#loginbutton")

  await popup.waitForSelector("[name=__CONFIRM__]")
  await popup.click("[name=__CONFIRM__]")
  await popup.click("[name=__CONFIRM__]")

  await page.waitForSelector("button[type='button']");
  const cookiesObject = await page.cookies()
  console.log(`Name: ${name}, FbId: ${fbid}`)
  console.log(cookiesObject)
  await browser.close();
};

Promise.all([
    login({ name: 'Login User one', fbid: '108109793879808', email: 'login_jgafcln_one@tfbnw.net', password: 'VXUru7oY5CD0v7nk' }),
    login({ name: 'Login User two', fbid: '108595553830510', email: 'login_wrewadg_two@tfbnw.net', password: 'VXUru7oY5CD0v7nk' }),
    login({ name: 'Login User three', fbid: '105273520832234', email: 'login_qdlwcrp_three@tfbnw.net', password: 'VXUru7oY5CD0v7nk' }),
    login({ name: 'Login User four', fbid: '104500907577643', email: 'login_ynjjppw_four@tfbnw.net', password: 'VXUru7oY5CD0v7nk' }),
    login({ name: 'Login User five', fbid: '101186341246183', email: 'login_bjohkde_five@tfbnw.net', password: 'VXUru7oY5CD0v7nk' })
]);